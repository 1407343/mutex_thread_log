#include "sig_link_main.h"
#include "link_func.h" 
#include "mutex_thread.h"
#include "printlog.h"
#include <unistd.h>

/*init_List*/
data_check *init_List()
{
	//assignment
	data_check *list = (data_check *)malloc(sizeof(data_check));

	list->head = NULL;
	list->tail = NULL;
	list->count = 0;
	list->f_compare = compare;
	list->f_delete = Delete_search;
	list->f_find = find_data;
	list->limit = 0;
	list->ckm_th = 0;
	list->end_th = 0;
	list->g_count = 0;
	
	return list;
}

int main()
{
	//init_list
	initLog();
	int c;// check number value
	
	data_check *list = init_List();
	while(1)
	{
		printf("How many want to make datas? (limit count is 1 ~ 100)=> ");
		c = scanf("%d",&list->limit);
		/*erase buffer*/
		if(c == 1 && list->limit > 0 && list->limit <= 100)//success number data
			break;
		FLUSH(stdin);
		if(list->limit > 100)
			PrintLog(WR,"limit count over",timestring());
			printf("limit count over\n");
		printf("check answer please\n");
	}


	//start program
	while(1)
	{
		//MENU menu = NON;		
		printf("what do you want\n"
				"select menu\n"
				"=================\n"
				"1. add mode\n"
				"2. delete mode\n"
				"3. find mode\n"
				"4. compare mode\n"
				"5. stack mode\n"
				"6. queue mode\n"
				"7. thread mode\n"
				"8. exit\n"
				"=================\n");
		printf("count : %d\n",list->count);
		printf("list->head : %p\n", list->head);
		int menu_num = 0;
		while(1)
		{
			printf("menu ===> ");
			c = scanf("%d",&menu_num);
		
			/*erase buffer*/
			if(c != 0 && menu_num > 0)//success number data
				break;
			FLUSH(stdin);
			printf("only insert number please\n");
		}
		//menu = menu_num;
		switch(menu_num)
		{
			case ADD:
				if(list->count >= list->limit)
				{
					printf("node is full\n"
							"You can't make node\n"
							"Delete another node\n");
					break;
				}
				printf("1. add node\n");
				Add(list);
				break;
			case DELETE:
				printf("2. delete node\n");
				Delete(list);
				break;
			case FIND:
			{
				printf("3. find node\n");
				
				printf("what do you want search?\n"
				"1. all data\n"
				"2. serach data\n"
				"3. exit menu\n");
				
				//FINMENU fmenu = FNON;
				int fmenu_num = 0;
				while(1)
				{
					printf("menu ===> ");
					c = scanf("%d",&fmenu_num);
		
					/*erase buffer*/
					if(c != 0 && menu_num > 0)//success number data
						break;
					FLUSH(stdin);
					printf("only insert number please\n");
				}
				//fmenu = fmenu_num;
				
				char name[MAX_SIZE] = { };
				if(fmenu_num == ONE)
				{
					printf("input name data => ");
					scanf("%s",name);
				}
				list->f_find(list, name, fmenu_num);
				/*reset value*/
				name[0] = '\0';
				fmenu_num = 0;
				break;
			}
			case COMPARE:
				printf("4. compare mode\n");
			    compared(list);		
				break;
			case STACK:
				printf("5. stack mode\n");
				stack(list);
				break;
			case QUEUE:
				printf("6. queue mode\n");
				queue(list);
				break;
			case THREAD:
				printf("7. thread mode\n");
				queue_menu(list);
				break;
			case CLOSE:
				printf("Exit\n");
				PrintLog(De, "EXIT PROGRAM", timestring());
				fclose(fp);
				goto END_MAIN_LOOP;
			default:
				printf("Wrong answer\n");
		}
		/*reset value*/	
		//menu = NON;
		menu_num = 0;
	}

END_MAIN_LOOP:
	/*all_data_delete*/
	list->head = NULL;
	list->tail = NULL;
	list->count = 0;
	list->limit = 0;
	list->f_compare = NULL;
	list->f_delete = NULL;
	list->f_find = NULL;

	free(list);//list free
	return 0;
}
