#include "sig_link_main.h" //use for struct data
#include "link_func.h"
#include "printlog.h"
#define MAX_SIZE 100

/*====================================================
 * AUTHOR : SEOSANGHO
 * DATE : 2021-05-27
 * THIS FILE IS DOUBLE LINK FUCTION EXAMPLE
=====================================================*/

/*==================================================
 * Add mode function place : 26 ~ 228
 * Delete mode function place : 349 ~ 638
 * compare mode : 230 ~ 341
 * find mode : 641 ~ 702 
 * stack mode : 705 ~ 828
 * queue mode : 830 ~ 952*/

/*Add_node
 * Add_First : �you can make head node
 * Add_Last : you can make tail node
 * Add_Pos : you can make node anywhere*/

/* you want make first node
 you can use this function*/
int Add_First(data_check *list,void *data)
{
	/*new node memory assignment*/
	node_t *node = (node_t*)malloc(sizeof(node_t));
	
	printf("node point %p\n",node);

	printf("sizeof(data) %d\n",(int)sizeof(data));

	node->name = (char*)data;
	printf("data => %s\n",(char*)node->name);
	printf("sizeof(node)->%d\n",(int)sizeof(node));
	printf("point data %p\n",&node->name);

	/*non_node*/
	if(list->head == NULL)
	{
		//first make head & tail
		printf("into first\n");
		list->head = node;
		list->tail = node;
		list->head->prev = NULL;
		list->tail->next = NULL;
		printf("list->head %p\n",list->head);
		printf("node %p\n",node);
	}
	else//node_exist
	{
		node->prev = list->head->prev;
		node->next = list->head;
		node->next->prev = node;
		
		list->head = node;
		printf("list->head %p\n",list->head);
		printf("node %p\n",node);
	}
	
	list->count++;

	return 0;
}

int Add_Last(data_check *list,void *data)
{
	/*new node memory assignment*/
	node_t *node = (node_t *)malloc(sizeof(node_t));

	printf("size_node->%d\n",(int)sizeof(node)); 
	
	node->name = (char*)data;

	/*non_node*/
	if(list->tail == NULL)
	{
		list->head = list->tail = node;
		list->head->prev = NULL;
		list->tail->next = NULL;
	}
	else/*exist node*/
	{	
		node->prev = list->tail;
		node->next = list->tail->next;
		list->tail->next = node;

		list->tail = node;
	}
	
	list->count++;

	return 0;
}

/*want replace*/
int Add_Pos(data_check* list, void *data, int place)
{
	/*check make node*/
	if(place - 1 <= list->count)
	{
		printf("OK, you can add this place\n");
	}
	else
	{
		printf("You can't add this place\n");
		return -1; // nope add pos
	}

	/*make node*/
	if(place == 1)//head place
	{
		printf("Add head\n");
		Add_First(list, data);
	}
	else if(place == (list->count + 1))//tail place
	{
		printf("Add tail\n");
		Add_Last(list, data);
	}
	else//anywhere
	{
		/*assigment node*/
		node_t *node = (node_t*)malloc(sizeof(node_t));

		node_t *cur;

		node->name = (char*)data;
		printf("data => %s\n", (char*)node->name);

		int	ckplace = place - 1; 
		cur = list->head;

		/*example : you want '2' place '1' place next is '2'place
					 you use one next. so, place -1 = use next amount*/

		/*search you want place point*/
		while(ckplace > 0)
		{
			cur = cur->next;
			ckplace--;
		}

		//resetting point
		node->prev = cur->prev;
		node->next = cur;
		node->name = (char*)data;

		cur->prev->next = node;
		cur->prev = node;

		++(list->count);

	}

	return 0;
}

void Add(data_check *list)
{
	/*select add menu*/
	/*ADD_DEL_MEN is define sig_link_main.h*/
	
	int ck;
	int menu = 0;
	//ADD_DEL_MEN emenu = ADNON;
	char names[MAX_SIZE];
	
	printf("What do you want add menu?\n"
			"1. First\n"
			"2. Last\n"
			"3. Another\n");

	while(1)
	{
		printf("menu ===> ");
		ck = scanf("%d",&menu);

		/*erase buffer*/
		if(ck != 0 && menu > 0)//success number data
			break;
		FLUSH(stdin);
		printf("only insert number please\n");
	}

	//emenu = menu;
	printf("Write your name\n");
	scanf("%s",names);
	char *name = (char*)malloc(sizeof(name));
	strcpy(name,names);

	switch(menu)
	{
		case FIRST:			
			Add_First(list, name);
			break;
		case LAST:
			Add_Last(list, name);
			break;
		case POS:
		{
			int place = 0;
			while(1)
			{
				printf("what do you want place\n");
				ck = scanf("%d",&place);

				/*erase buffer*/
				if(ck != 0 && place > 0)//success number data
					break;
				FLUSH(stdin);
				printf("only insert number please\n");
			}
			Add_Pos(list, name, place);
			place = 0;
			break;
		}
		default:
			printf("You answer wrong\n");
			break;
	}
	/*reset value*/
	//emenu = ADNON;
	menu = 0;

}

/*retrun long data*/
void compared(data_check *list)
{
	node_t *cur;
	cur = list->head;

	if(list->count < 2)
	{
		printf("We don't have compare data\n");
		return;
	}
	
	printf("where place data compare?\n");
	int place1 = 0;
	int place2 = 0;
	int ck = 0;
	while(1)
	{
		printf("write place 1 (ex. 1 or 2) => ");
		ck = scanf("%d",&place1);

		if(ck != 0 && place1 > 0)//success number data
			break;
		FLUSH(stdin);//erase buff
		printf("only insert number please\n");
	}
	while(1)
	{
		printf("write place 2 (ex. 1 or 2) => ");
		ck = scanf("%d",&place2);

		if(ck != 0 && place2 > 0)//success number data
			break;
		FLUSH(stdin);
		printf("only insert number please\n");
	}

	if(place1 <= list->count && place2 <= list->count)
	{
		printf("OK, you can compare this place\n");
	}
	else
	{
		perror("You can't compare this place\n"); 
		return;
	}
	
	for(; cur != NULL ; cur = cur->next)
	{
		if(place1 - 1 == 0)
		{
			printf("=======================\n");
			printf("address : %p\n",cur);
			printf("data : %s\n",(char*)cur->name);
			printf("=======================\n");
			break;
		}
		place1--;
	}
	
	void *data1 = cur->name;
	cur = list->head;

	for(; cur != NULL ; cur = cur->next)
	{
		if(place2 - 1 == 0)
		{
			printf("=======================\n");
			printf("address : %p\n",cur);
			printf("data : %s\n",(char*)cur->name);
			printf("=======================\n");
			break;
		}
		place2--;
	}

	void *data2 = cur->name;
	
	list->f_compare(data1, data2);
}

int compare(void *name1, void *name2)
{
	/*check input data*/
	if(name1 == NULL || name2 == NULL)
	{
		printf("You don't have address data\n");
		return -1;//non_data
	}

	/*compare data length*/
	int c_result = strcmp((char*)name1, (char*)name2);
	
	if(c_result == 0)//same data
	{
		printf("same data for ASCII\n");
		return 0;
	}
	else if(c_result > 0)//name1 data is long
	{
		printf("first data is ASCII big or long data\n");
		return 1;//first data
	}
	else//name2 data is long
	{
		printf("second data is ASCII big or long data\n");
		return 2;//second data
	}

	return -2; //error
}
/*delete_node
 * Delete_First : you can delete first data & change head
 * Delete_Last : you can last data & change tail
 * Delete_pos : you can delete node anywhere*/

/*delete_data*/
int Delete_First(data_check *list)
{
	node_t *cur; // find point place
	node_t *node;

	node = list->head;
	
	//find & delete data 
	if(node == NULL)
	{
		perror("You don't have data\n");
		return -1; //error
	}
	else
	{
		if(list->count == 1)
		{
			list->head = NULL;
			list->tail = NULL;
															
			free(node->name);
			free(node);

			--list->count;
		}
		else
		{
			/*change head*/
			cur = list->head->next; //find second node
			cur->prev = NULL;
			list->head = cur;

			/*data delete*/
			node->prev = NULL;
			node->next = NULL;

			free(node->name);
			free(node);
		
			--list->count;
		}
	}

	return 0; //success
}

int Delete_Last(data_check *list)
{
	node_t *cur;
	node_t *node;

	node = list->tail;

	//find & delete data
	if(node == NULL)
	{
		perror("You don't have data\n");
		return -1; //error
	}
	else
	{
		if(list->count == 1)
		{
			list->head = NULL;
			list->tail = NULL;
			
			free(node->name);
			free(node);

			--list->count;
		}
		else
		{
			/*change head*/
			cur = list->tail->prev; //find new tail
			cur->next = NULL;
			list->tail = cur;

			/*data delete*/
			node->prev = NULL;
			node->next = NULL;

			free(node->name);
			free(node);

			--list->count;
		}
	}

	return 0; // success
}

int Delete_pos(data_check *list, int place)
{
	/*check make node*/
	if(place <= list->count && list->count > 0)
	{
		printf("OK, you can delete this place\n");
	}
	else
	{
		perror("You can't delete this place\n");
		return -1; // nope add pos
	}
	
	node_t *node;
	node_t *cur;
	
	/*delete node*/
	if(place == 1)//head place
	{
		printf("Delete head\n");
		Delete_First(list);
	}
	else if(place == list->count)//tail place
	{
		printf("Delete tail\n");
		Delete_Last(list);
	}
	else//anywhere
	{
		int	ckplace = place - 1; 
		cur = list->head;

		/*search you want place point*/
		while(ckplace > 0)
		{
			cur = cur->next;
			ckplace--;
		}

		//resetting point
		node = cur;
		cur->next->prev = cur->prev;
		cur->prev->next = cur->next;

		node->next = NULL;
		node->prev = NULL;

		free(node->name);
		free(node);

		--list->count;
	}

	return 0;
}

void Delete_search(data_check *list, void *data)
{
	node_t *node;
	node_t *cur;
	cur = list->head;
	int su_check = 0;

	for(; cur != NULL ; cur = cur->next)
	{
		if(strcmp((char *)cur->name,(char*)data) == 0 )
		{
			if(list->head == cur && list->tail != cur && list->count != 1)
			{
				Delete_First(list);
				printf("success delete data\n");
				su_check = 1;
			}
			else if(list->tail == cur && list->head != cur && list->count != 1)
			{
				Delete_Last(list);
				printf("success delete data\n");
				su_check = 1;
			}
			else if(list->count == 1)
			{
				node = cur;

				list->head = NULL;
				list->tail = NULL;
				
				free(node->name);
				free(node);

				--list->count;
				printf("success delete data\n");
				su_check = 1;
				
			}
			else
			{
				//resetting point
				node = cur;
				cur->next->prev = cur->prev;
				cur->prev->next = cur->next;

				node->name = NULL;
				node->next = NULL;
				node->prev = NULL;

				free(node->name);
				free(node);

				--list->count;
				printf("success delete data\n");
				su_check = 1;
			}
		}
	}
	if(su_check != 1)
		printf("we don't have this data\n");
}

/*Delete inter face*/
void Delete(data_check *list)
{
	/*select add menu*/
	/*ADD_DEL_MEN is define sig_link_main.h*/
	if(list->count < 1)
	{
		printf("we don't have data\n");
		return;
	}
	//ADD_DEL_MEN emenu = ADNON;
	
	char names[MAX_SIZE];
	
	printf("What do you want add menu?\n"
			"1. First\n"
			"2. Last\n"
			"3. Place\n"
			"4. search\n");

	int menu = 0;
	int ck = 0;
	while(1)
	{
		printf("menu => ");
		ck = scanf("%d",&menu);

		/*erase buffer*/
		if(ck != 0 && menu > 0)//success number data
			break;
		FLUSH(stdin);
		printf("only insert number please\n");
	}
	
	//emenu = menu;
	
	if(menu == 4)
	{
		printf("Write your name\n");
		scanf("%s",names);
	}
	void *name = names;
	switch(menu)
	{
		case FIRST:			
			Delete_First(list);
			break;
		case LAST:
			Delete_Last(list);
			break;
		case POS:
		{
			int place = 0;
			printf("what do you want place?\n");
			while(1)
			{
				printf("menu => ");
				ck = scanf("%d",&place);

				/*erase buffer*/
				if(ck != 0 && place > 0)//success number data
				break;
				FLUSH(stdin);
				printf("only insert number please\n");
			}		
			Delete_pos(list, place);
			place = 0;
			break;
		}
		case SEARCH:
			list->f_delete(list, name);
			break;
		default:
			printf("You write wrong num\n");
			break;
	}
	/*reset value*/
	menu = 0;
	//emenu = ADNON;
}

/*find_print_data*/
void find_data(data_check *list, void *name, int fmenu)
{
	if(list->count <= 0)
	{
		printf("we don't have data\n");
		return;
	}
	node_t *cur;
	cur = list->head;
	printf("head point%p\n",list->head);
	printf("head point%p\n",cur);
	printf("head data %s\n",(char*)cur->name);

	switch(fmenu)
	{
		case 1:
		{	
			int data_num = 1;
			for(; cur != NULL ; cur = cur->next)
			{
				printf("============================\n");
				printf("data_number : %d\n",data_num);
				printf("address : %p\n",cur);
				printf("data : %s\n", (char*)cur->name);
				printf("data point : %p\n",&cur->name);
				printf("============================\n");
				data_num++;
			}
			break;
		}
		case 2:
		{
			int ck_data = 0;
			for(; cur != NULL ; cur = cur->next)
			{
				if(strcmp((char*)cur->name,(char*)name) == 0)
				{
					printf("=======================\n");
					printf("address : %p\n",cur);
					printf("data : %s\n",(char*)cur->name);
					printf("=======================\n");
					fmenu = F_CLOSE;
					ck_data = 1;
				}

			}
			if(ck_data == 0)
			{
				printf("we don't have this data\n");
				ck_data = 0;
			}
			break;
		}
		case 3:
			break;
		
		default:
			printf("wrong answer menu\n");
			break;
	}
	
	fmenu = 0; //reset value
}

int stack(data_check *list)
{
	while(1)
	{
		int ck = 0;
		int menu = 0;

		printf("what do you want menu?\n"
			"1. PUSH\n"
			"2. POP\n"
			"3. end menu\n");

		while(1)
		{	
			printf("menu => ");
			ck = scanf("%d",&menu);

			/*erase buffer*/
			if(ck != 0 && menu > 0)//success number data
			break;
			FLUSH(stdin);
			printf("only insert number please\n");
		}
		if(menu == 1)	
		{
			if(list->count >= list->limit)
			{
				printf("data is Full\n");
				break;
			}
			printf("write data=> ");
			char name[MAX_SIZE] = { };
			scanf("%s",name);
			char *data = (char*)malloc(sizeof(name));
			strcpy(data,name);

			/*new node memory assignment*/
			node_t *node = (node_t *)malloc(sizeof(node_t));

			printf("size_node->%d\n",(int)sizeof(node)); 
	
			node->name = data;
	
			/*non_node*/
			if(list->tail == NULL)
			{
				list->head = list->tail = node;
				list->head->prev = NULL;
				list->tail->next = NULL;
			}	
			else/*exist node*/
			{	
				node->prev = list->tail;
				node->next = list->tail->next;
				list->tail->next = node;

				list->tail = node;
			}	
		
			list->count++;
			printf("success push\n");
			break;
		}
		else if(menu == 2)
		{
			node_t *cur;
			node_t *node;

			node = list->tail;

			//find & delete data
			if(node == NULL)
			{
				perror("You don't have data\n");
				break; //error
			}
			else
			{
				if(list->count == 1)
				{
					list->head = NULL;
					list->tail = NULL;
			
					free(node->name);
					free(node);

					--list->count;
					printf("success pop\n");
					break;
				}
				else
				{
					/*change head*/
					cur = list->tail->prev; //find new tail
					cur->next = NULL;
					list->tail = cur;

					/*data delete*/
					node->prev = NULL;
					node->next = NULL;

					free(node->name);
					free(node);

					--list->count;
					printf("success pop\n");
					break;
				}
			}
	
		}
		else if(menu == 3)
		{
			printf("exit stack menu\n");
			return 0;
		}
		else
		{
			printf("check answer\n");
		}
	}

	return 0;
}

int queue(data_check *list)
{
	while(1)
	{
		int ck = 0; // check num
		int menu = 0;

		printf("what do you want menu?\n"
			"1. addq\n"
			"2. deleteq\n"
			"3. end menu\n");

		while(1)
		{	
			printf("menu => ");
			ck = scanf("%d",&menu);

			/*erase buffer*/
			if(ck != 0 && menu > 0)//success number data
			break;
			FLUSH(stdin);
			printf("only insert number please\n");
		}
		if(menu == 1)	
		{
			if(list->count >= list->limit)
			{
				printf("data is Full\n");
				break;
			}
			printf("write data=> ");
			char name[MAX_SIZE] = { };
			scanf("%s",name);
			char *data = (char*)malloc(sizeof(name));
			strcpy(data,name);
			
			/*new node memory assignment*/
			node_t *node = (node_t *)malloc(sizeof(node_t));

			printf("size_node->%d\n",(int)sizeof(node)); 
	
			node->name = data;
	
			/*non_node*/
			if(list->head == NULL)
			{
				list->head = list->tail = node;
				list->head->prev = NULL;
				list->tail->next = NULL;
			}	
			else/*exist node*/
			{	
				node->next = list->head;
				node->prev = NULL;
				list->head->prev = node;

				list->head = node;
			}	
		
			list->count++;
			printf("success push\n");
			break;
		}
		else if(menu == 2)
		{
			node_t *cur; // find point place
			node_t *node;
	
			node = list->head;
	
			//find & delete data 
			if(node == NULL)
			{
				perror("You don't have data\n");
				return -1; //error
			}
			else
			{
				if(list->count == 1)
				{
					list->head = NULL;
					list->tail = NULL;
															
					free(node->name);
					free(node);

					--list->count;
					printf("success delete q\n");
					break;
				}
				else
				{
					/*change head*/
					cur = list->head->next; //find second node
					cur->prev = NULL;
					list->head = cur;

					/*data delete*/
					node->prev = NULL;
					node->next = NULL;

					free(node->name);
					free(node);
		
					--list->count;
					printf("success delete q\n");
					break;
				}
			}
		}
		else if(menu == 3)
		{
			printf("exit qmeun\n");
			return 0;
		}
		else
		{
			printf("check answer\n");
		}
	}
	
	return 0;
}
