#pragma once
#include <stdio.h>
#include <stdio_ext.h>
#define MAX_SIZE 100
#define FLUSH(fp) __fpurge(fp)

/*start stop*/
typedef enum
{
	FALSE,
	TRUE
}GO;

/*select menu*/
typedef enum
{
	NON = 0,
	ADD,
	DELETE,
	FIND,
	COMPARE,
	STACK,
	QUEUE,
	THREAD,
	CLOSE
}MENU;

typedef enum
{
	FNON = 0,
	ALL = 1,
	ONE,
	F_CLOSE
}FINMENU;


typedef enum
{
	ADNON = 0,
	FIRST = 1,
	LAST,
	POS,
	SEARCH,
}ADD_DEL_MEN;

/* struct data node*/
typedef struct member_list{
	void *name;
	struct member_list *prev;
	struct member_list *next;
}node_t;

/* struct information*/
typedef struct check_member{
	node_t *head;
	node_t *tail;
	int count;
	int limit;
	int ckm_th;
	int end_th;
	int g_count;
	int (*f_compare)(void *name1, void *name2);
	void (*f_delete)(struct check_member *list ,void *name);
	void (*f_find)(struct check_member *list ,void *name, int fmenu);
}data_check;
