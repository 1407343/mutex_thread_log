#pragma once
#include "sig_link_main.h" //use for struct data
#include "link_func.h"
#include <pthread.h>

int queue_menu(data_check *list);
void *create_thread(void *data);
void *delete_thread(void *data);
void *print_thread(void *data);
void *ran_delete_thread(void *data);
void *ran_create_thread(void *data);
void *ran2_delete_thread(void *data);
void *ran2_create_thread(void *data);
