#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <pthread.h> // use thread

#include <sys/time.h> //use time
#include <time.h>

#define LL 5 // 5 loglevel
#define CH 256 // log_contents max
#define MB2 2048 // 1MB = 1024byte

#define MUTEX_LOCK() pthread_mutex_lock(&gLogMutex);
#define MUTEX_UNLOCK() pthread_mutex_unlock(&gLogMutex);

extern int gfile_num;
extern FILE* fp;

//log file print level
enum {
	De = 0, //default
	WR, //warning
	ER, //error
	SCS, //sucess
	IFM // information
};

int initLog(); // log init function
char *timestring(); // current into string
int PrintLog(int lglevel, char* log_content, char* logtime);
