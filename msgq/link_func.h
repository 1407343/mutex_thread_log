#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "sig_link_main.h"
#include "mutex_thread.h"

/*link_func header file
 * You want define function go to the sig_link.c*/

/*init list*/
//data_check *init_List();

/*Add_data func*/
int Add_First(data_check *list, void *data);
int Add_Last(data_check *list, void *data);
int Add_Pos(data_check *list, void *data, int place);

/*Add_node_interface*/
void Add(data_check *list);

/*Delete_data func*/
int Delete_first(data_check *list);
int Delete_Last(data_check *list);
int Delete_pos(data_check *list, int place);
void Delete_search(data_check *list ,void *data);

/*Delete_node_interface*/
void Delete(data_check *list);

/*Research_data func*/
void find_data(struct check_member *list, void *name, FINMENU fmenu);

/*compare_data func*/
void compared(data_check *list);
int compare(void *name1, void *name2);

/*stack func*/
int stack(data_check *list);
int queue(data_check *list);
