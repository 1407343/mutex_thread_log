#include "mutex_thread.h"
#include <stdint.h>
#include <unistd.h>
#define MAX_TH 1000 

/*MAX thread is 10(add thread : 4 , delete thread : 4 , print thread : 2)*/
pthread_t add_th[4];
pthread_t del_th[4];
pthread_t prt_th[2];

/*MAX ran thread is 10(add thread : 5, delete thread : 5)*/
pthread_t ran_addth[5];
pthread_t ran_delth[5];

int add_thnum = 0;
uint32_t del_thnum = 0;
uint32_t prt_thnum = 0;

uint32_t th_result = 0;
uint32_t th_success = 0;
int init = 0;
int g_state = 0;//non :0 start:1
int end_thread = 0;

int min = 0;
int add = 0;

pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t ad_cond = PTHREAD_COND_INITIALIZER;
pthread_cond_t del_cond = PTHREAD_COND_INITIALIZER;
pthread_cond_t prt_cond = PTHREAD_COND_INITIALIZER;

pthread_cond_t add_cond = PTHREAD_COND_INITIALIZER;
pthread_cond_t minus_cond = PTHREAD_COND_INITIALIZER;

pthread_cond_t ran_cond = PTHREAD_COND_INITIALIZER;

/*thread interface*/
int queue_menu(data_check *list)
{
	while(1)
	{
		int ck = 0; // check num
		int menu = 0;
		printf("========================\n");
		printf("what do you want menu?\n"
			"1. make_add_thread\n"
			"2. make_delete_thread\n"
			"3. make_print_thread\n"
			"4. join_add_thread\n"
			"5. join_delete_thread\n"
			"6. join_print_thread\n"
			"7. random add_thread & delete_thread & data limit\n"
			"8. random add_thread & delete_thread(non datalimit\n"
			"9. end menu\n");
		printf("=======================\n");

		while(1)
		{	
			printf("menu => ");
			ck = scanf("%d",&menu);

			/*erase buffer*/
			if(ck != 0 && menu > 0)//success number data
			break;
			FLUSH(stdin);
			printf("only insert number please\n");
		}
//		if(menu == 1)	
//		{
//			if(add_thnum >= 4)
//			{
//				printf("add_thread is Full\n");
//				break;
//			}
//			int thr_ch = 0;
//			thr_ch = pthread_create(&add_th[add_thnum], NULL, create_thread,(void*)list);
//			if (thr_ch < 0 )
//			{
//				perror("add_thread create error : ");
//				exit(0);
//			}
//			printf("make %u add_thread\n",add_thnum+1);
//			add_thnum++;
//
//		}
//		else if(menu == 2)
//		{
//			if(del_thnum >= 4)
//			{
//				printf("delete_thread is Full\n");
//				break;
//			}
//			int thr_ch = 0;
//			thr_ch = pthread_create(&del_th[del_thnum], NULL, delete_thread,(void*)list);
//			if(thr_ch < 0)
//			{
//				perror("del_thread create error : ");
//				exit(0);
//			}
//			printf("make %u del_thread\n",del_thnum);
//			del_thnum++;
//		}
//		else if(menu == 3)
//		{
//			if(prt_thnum >= 2)
//			{
//				printf("print_thread is Full\n");
//				break;
//			}
//			int thr_ch = 0;
//			thr_ch = pthread_create(&prt_th[prt_thnum], NULL, print_thread,(void*)list);
//
//			if(thr_ch < 0)
//			{
//				perror("prt_thread create error : ");
//				exit(0);
//			}
//			printf("make %u prt_thread\n",prt_thnum);
//			prt_thnum++;
//		}
//		else if(menu == 4)
//		{
//			if(add_thnum > 0)
//			{
//				int i = 0;
//				for(; i < add_thnum; i++)
//				{
//					pthread_cond_broadcast(&ad_cond);
//					pthread_join(add_th[i],0);
//				}
//				add_thnum = 0;
//				list->ckm_th = 0;
//			}
//			else
//			{
//				printf("We don't have add_thread\n");
//			}
//		}
//		else if(menu == 5)
//		{
//			if(del_thnum > 0)
//			{
//				int i = 0;
//				for(; i < del_thnum; i++)
//				{
//					pthread_cond_broadcast(&del_cond);
//					pthread_join(del_th[i],0);
//					printf("success delete data\n");
//				}
//				del_thnum = 0;
//			}
//			else
//			{
//				printf("we don't have del_thread\n");
//			}
//		}
//		else if(menu == 6)
//		{
//			if(prt_thnum > 0)
//			{
//				int i = 0;
//				for(; i < prt_thnum; i++)
//				{
//					pthread_cond_broadcast(&prt_cond);
//					pthread_join(prt_th[i],0);
//					if(th_result == 0)
//						printf("success print data\n");
//				}
//				prt_thnum = 0;
//			}
//			else
//			{
//				printf("we don't have print_thread\n");
//			}
//		}
		if(menu == 7)
		{
			if(list->ckm_th > 20)
				list->ckm_th = 0;//reset ckm_th
			int ran_add = 0;
			int ran_del = 0;
			int maketh = 0;
			int c = 0;

			//checking making thread
			while(1)
			{
				printf("what do you want make add thread?\n"
						"You can make 1 ~ 5 add thread => ");
				c = scanf("%d",&ran_add);
				if(c == 1 && ran_add > 0 && ran_add <= 5)//success number data
					break;
				FLUSH(stdin);
				printf("check answer please\n");

			}
			while(1)
			{
				printf("what do you want make add thread?\n"
						"You can make 1 ~ 5 add thread => ");
				c = scanf("%d",&ran_del);
				if(c == 1 && ran_del > 0 && ran_del <= 5)//success number data
					break;
				FLUSH(stdin);
				printf("check answer please\n");
			}
			
			for(maketh = 0; maketh < ran_add ; maketh++)
			{
				pthread_create(&ran_addth[maketh], NULL, ran_create_thread,(void*)list);
			}
			for(maketh = 0; maketh < ran_del ; maketh++)
			{
				pthread_create(&ran_delth[maketh], NULL, ran_delete_thread,(void*)list);
			}
			int total_th = ran_add + ran_del;
			while(1)
			{
				sleep(1);
				printf("list->end_th %d\n",list->end_th);
				printf("total_th %d\n",total_th);
				if(list->g_count >= MAX_TH)
				{
					pthread_cond_broadcast(&add_cond);
					pthread_cond_broadcast(&minus_cond);
				}
				if(total_th == list->end_th)
				{
					list->g_count = 0;
					list->end_th = 0;
					break;
				}
			}
		}
		else if(menu == 8)
		{
			if(list->ckm_th > 20)
				list->ckm_th = 0;//reset ckm_th
			int ran_add = 0;
			int ran_del = 0;
			int maketh = 0;
			int c = 0;

			//checking making thread
			while(1)
			{
				printf("what do you want make add thread?\n"
						"You can make 1 ~ 5 add thread => ");
				c = scanf("%d",&ran_add);
				if(c == 1 && ran_add > 0 && ran_add <= 5)//success number data
					break;
				FLUSH(stdin);
				printf("check answer please\n");

			}
			while(1)
			{
				printf("what do you want make add thread?\n"
						"You can make 1 ~ 5 delete thread => ");
				c = scanf("%d",&ran_del);
				if(c == 1 && ran_del > 0 && ran_del <= 5)//success number data
					break;
				FLUSH(stdin);
				printf("check answer please\n");
			}
			for(maketh = 0; maketh < ran_del ; maketh++)
			{
				pthread_create(&ran_delth[maketh], NULL, ran2_delete_thread,(void*)list);
				sleep(1);
			}
			for(maketh = 0; maketh < ran_add ; maketh++)
			{
				pthread_create(&ran_addth[maketh], NULL, ran2_create_thread,(void*)list);
			}
			int total_th = ran_add + ran_del;
			while(1)
			{
				sleep(1);
				printf("list->end_th %d\n",list->end_th);
				printf("total_th %d\n",total_th);
				if(total_th == list->end_th)
				{
					list->g_count = 0;
					list->end_th = 0;
					break;
				}
			}

		}
		else if(menu == 9)
		{
			printf("exit qmeun\n");
			return 0;
		}
		else
		{
			printf("check answer\n");
			printf("we have %u add_thread\n", add_thnum);
			printf("%u delete_thread\n", del_thnum);
			printf("%u prt_thnum\n", prt_thnum);
			printf("----------------------------\n");
		}
	}
	
	return 0;
}

/*thread function*/

//void *create_thread(void* data)
//{
//	int add_num = 0;
//	data_check *ch_list = (data_check*)data;
//	printf("ch_list => %d\n",ch_list->count);
//	
//	pthread_cond_wait(&ad_cond,&mutex);
//	data_check *list = (data_check*)data;
//	if(ch_list->count == list->count)
//	{
//		printf("o�oh s");
//		printf("ch_list_count : %d\n", ch_list->count);
//		printf("list->count : %d\n", list->count);
//	}
//	if(list->count > 0)
//	{
//		printf("another thread check list->head : %p\n",list->head);
//	}
//	for(; add_num < 4 ; add_num++)
//	{
//		printf("into thread check list->head : %p\n",list->head);
//		pthread_t id;
//		id = pthread_self();
//		printf("Thread id is %ud\n", (int)id);
//		if(list->count >= list->limit)
//		{
//			printf("data is Full\n");
//			break;
//		}
//		printf("This is add_thread\n");
//		int ckm = list->ckm_th;
//		char name1[MAX_SIZE] = "thread data ";
//		char name2[MAX_SIZE] = {};
//		char name3[MAX_SIZE] = {};
//		sprintf(name2, "%d", ckm);
//		sprintf(name3, "%d", add_num);
//		strcat(name1,name2);
//		strcat(name1,name3);
//		void *name = (char*)malloc(sizeof(name1));
//		strcpy(name,name1);
//
//		/*new node memory assignment*/
//		node_t *node = (node_t *)malloc(sizeof(node_t));
//		node->name = name;
//		printf("this node data -> %s\n",(char*)node->name);
//		printf("node locataion -> %p\n",node);
//
//		/*non_node*/
//		if(list->tail == NULL)
//		{
//			list->head = list->tail = node;
//			list->head->prev = NULL;
//			list->tail->next = NULL;
//		}
//		else/*exist node*/
//		{	
//			node->prev = list->tail;
//			node->next = NULL;
//			list->tail->next = node;
//
//			list->tail = node;
//		}
//		
//		list->count++;
//		if(list->count > 1)
//			printf("check head next data : %p\n",list->head->next);
//		printf("\n");
//		printf("success push\n");
//	}
//	printf("end add this thread\n");
//	printf("into thread last list->head : %p\n",list->head);
//	printf("======================\n");
//	sleep(1);
//	printf("into thread last list->count : %d\n",list->count);
//	list->ckm_th++;
//
//	pthread_mutex_unlock(&mutex);
//	return (void*)list;
//}
//void *delete_thread(void* data)
//{
//	int del_num = 0;
//	data_check* list = data;
//	printf("%d delete thread delete ready\n",del_thnum);
//	pthread_cond_wait(&del_cond, &mutex);	
//	for(; del_num < 4 ; del_num++)
//	{
//		if(list->count <= 0)
//		{	
//			printf("you dont have data\n");
//			pthread_mutex_unlock(&mutex);
//			return (void*)list;
//		}
//		pthread_t id;
//		id = pthread_self();
//		printf("Thread id is %ud\n", (int)id);
//	
//		printf("This is delete_thread\n");
//		node_t *cur; // find point place
//		node_t *node;
//
//		node = list->head;
//
//		//find & delete data 
//		if(node == NULL)
//		{
//			perror("You don't have data\n");
//			printf("or thread is complete all delete data\n");
//			th_success = -1;
//			pthread_mutex_unlock(&mutex);
//			return (void*)list; //error
//		}
//		else
//		{
//			if(list->count == 1)
//			{
//				list->head = NULL;
//				list->tail = NULL;
//			
//				printf("delete %s data\n",(char*)node->name);
//				free(node->name);
//				free(node);
//
//				--list->count;
//				printf("success delete q\n");
//				th_success = 0;
//			}
//			else if(list->count == 0)
//			{
//				printf("you dont have data\n");
//				pthread_mutex_unlock(&mutex);
//				return (void*)list;
//			}
//			else
//			{
//				/*change head*/
//				cur = list->head->next; //find second node
//				cur->prev = NULL;
//				list->head = cur;
//
//				/*data delete*/
//				node->prev = NULL;
//				node->next = NULL;
//
//				printf("delete %s data\n",(char*)node->name);
//				free(node->name);
//				free(node);
//		
//				--list->count;
//				printf("success delete q\n");
//				printf("\n");
//			}	
//		}
//	}
//	printf("this thread is success 4 data delete\n");
//	pthread_mutex_unlock(&mutex);	
//	return (void*)list;
//}
//void *print_thread(void* data)
//{
//	data_check* list = (data_check*)data;
//	pthread_cond_wait(&prt_cond, &mutex);
//	if(list->count == 0)
//	{
//		printf("we dont have data\n");
//		th_success = -1;
//		pthread_mutex_unlock(&mutex);
//		return (void*)list;
//	}
//	if(prt_thnum == 1)
//	{
//		node_t *node;
//
//		node = list->head;
//		int num = 0;
//		for(;node != NULL;node = node->next)
//		{	
//			pthread_t id;
//			id = pthread_self();
//			printf("===========================\n");
//			printf("Thread id is %ud\n", (int)id);
//			printf("data : %s\n", (char*)node->name);
//			printf("data_num : %d\n", num);
//			printf("===========================\n");
//			num++;
//		}
//		printf("end check data this thread\n");
//		pthread_mutex_unlock(&mutex);
//		sleep(1);
//	}
//	else if(prt_thnum == 2)
//	{
//		node_t *node;
//		int num = list->count;
//		node = list->tail;
//		for(;node != NULL ; node = node->prev)
//		{
//			pthread_t id;
//			id = pthread_self();
//			printf("===========================\n");
//			printf("Thread id is %ud\n", (int)id);
//			printf("data : %s\n", (char*)node->name);
//			printf("data_num : %d\n", num);
//			printf("===========================\n");
//			num--;
//			printf("end check data this thread\n");
//			pthread_mutex_unlock(&mutex);
//		}
//		sleep(1);
//	}
//	return (void*)list; 
//}



//random create & delete function
void *ran_create_thread(void* data)
{
	pthread_t id;
	id = pthread_self();
	data_check *list = (data_check*)data;
	printf("%d add thread ready\n",(int)id);
	list->ckm_th++;//thread number
	
	int ckm = list->ckm_th;
	int add_num = 0;

	while(list->g_count < MAX_TH)
	{
		pthread_mutex_lock(&mutex);
	
		//update data
		list = (data_check*)data;
		
		printf("\n");
		printf("===this is checking data===\n");
		printf("list->count : %d\n", list->count);

		//check_max_count
		if(list->count == list->limit && MAX_TH > list->g_count)
		{
			add = 1;
			printf("into wait parphome");
			pthread_cond_wait(&add_cond,&mutex);
			list = (data_check*)data;
		}
		if(list->g_count >= MAX_TH)
		{
			pthread_cond_broadcast(&minus_cond);
			printf("you are mission complete\n");
			break;
		}

		//make data & id 
		printf("Thread id is %ud\n", (int)id);
		printf("This is add_thread\n");
		char name1[MAX_SIZE] = "thread data ";
		char name2[MAX_SIZE] = {};
		char name3[MAX_SIZE] = {};
		sprintf(name2, "%d", ckm);
		sprintf(name3, " %d", add_num);
		strcat(name1,name2);
		strcat(name1,name3);
		void *name = (char*)malloc(sizeof(name1));
		strcpy(name,name1);

		/*new node memory assignment*/
		node_t *node = (node_t *)malloc(sizeof(node_t));
		node->name = name;
		printf("this node data -> %s\n",(char*)node->name);
		printf("node locataion -> %p\n",node);
		printf("\n");

		/*non_node*/
		if(list->tail == NULL)
		{
			list->head = list->tail = node;
			list->head->prev = NULL;
			list->tail->next = NULL;
		}
		else/*exist node*/
		{	
			node->prev = list->tail;
			node->next = NULL;
			list->tail->next = node;

			list->tail = node;
		}
		
		list->count++;
		list->g_count++;//use thread amount
		add_num++;//make data number
		
		if(list->count > 1)
			printf("check head next data : %p\n",list->head->next);
		printf("success push\n");
		printf("check thread going : %d\n",list->g_count);
		if(0 < list->count && min == 1)
		{
			pthread_cond_broadcast(&minus_cond);
			printf("check data full into menu\n");
			min = 0;
		}

		pthread_mutex_unlock(&mutex);
		printf("=========end one add while============\n");
		printf("\n");

	}

	printf("end add this thread\n");
	printf("into thread last list->head : %p\n",list->head);
	printf("into thread last list->count : %d\n",list->count);
	printf("====================\n");
	printf("\n");

	pthread_mutex_unlock(&mutex);
	list->end_th++;
	return NULL;
}

void *ran_delete_thread(void* data)
{
	pthread_t id;
	id = pthread_self();
	printf("%d delete thread ready\n",(int)id);

	//wait for start signal
	//pthread_cond_wait(&start,&mutex);
	
	data_check* list = data;
	while(list->g_count < MAX_TH)
	{
		pthread_mutex_lock(&mutex);
		//first check
		while(list->count <= 0 && MAX_TH > list->g_count)
		{	
			if(list->head == NULL)
			{
				printf("=====first check non data====\n");
				printf("you dont have data\n");
				printf("wait and another thread start\n");
				printf("check thread going %d\n",list->g_count);
				printf("====this thread is wait first check non data====\n");
				min = 1;
				pthread_cond_wait(&minus_cond,&mutex);//stop this thread(into mutex unlock form)
			}
		}
		if(list->g_count >= MAX_TH)
		{
			pthread_cond_broadcast(&add_cond);
			printf("you are mission complete\n");
			break;
		}
	
		list = (data_check*)data; //update data
		
		printf("Thread id is %ud\n", (int)id);
		
		node_t *cur; // find point place
		node_t *node;

		node = list->head;

		if(list->count == 1)
		{
			list->head = NULL;
			list->tail = NULL;
			printf("this is delete node point  %p\n",node);	
			printf("delete %s data\n",(char*)node->name);
			printf("\n");

			free(node->name);
			free(node);

			--list->count;
			printf("success delete q\n");
		}
		else
		{
			/*change head*/
			cur = list->head->next; //find second node
			cur->prev = NULL;
			list->head = cur;

			/*data delete*/
			node->prev = NULL;
			node->next = NULL;

			printf("delete %s data\n",(char*)node->name);
			free(node->name);
			free(node);
		
			--list->count;
			printf("success delete q\n");
		}	
		list->g_count++;//execution thread amount
		printf("thread going check : %d\n",list->g_count);
		printf("=========================\n");
		printf("\n");
		printf("======and one delete=========\n");
		printf("\n");
		if(list->count < list->limit && add == 1)//if full data -> data minuse-> alram data is not full
		{
			pthread_cond_broadcast(&add_cond);
			printf("delete mode ->add mode open\n");
			add = 0;
		}
		pthread_mutex_unlock(&mutex);
	}
	
	printf("end delete thread\n");
	printf("===========\n");
	printf("\n");
	pthread_mutex_unlock(&mutex);
	list->end_th++;
	return NULL;
}


//random create & delete function
void *ran2_create_thread(void* data)
{
	pthread_t id;
	id = pthread_self();
	data_check *list = (data_check*)data;
	list->ckm_th++;//thread number
	
	int ckm = list->ckm_th;
	int add_num = 0;

	while(1)
	{
		if(list->g_count < MAX_TH)
		{
			//make data & id 
			char name1[MAX_SIZE] = "thread data ";
			char name2[MAX_SIZE] = {};
			char name3[MAX_SIZE] = {};
			sprintf(name2, "%d", ckm);
			sprintf(name3, " %d", add_num);
			strcat(name1,name2);
			strcat(name1,name3);
			void *name = (char*)malloc(sizeof(name1));
			strcpy(name,name1);

			/*new node memory assignment*/
			node_t *node = (node_t *)malloc(sizeof(node_t));
			node->name = name;
			
//			if(list->g_count >= MAX_TH)
//			{
//				pthread_mutex_unlock(&mutex);
//				break;
//			}
			/*non_node*/
			pthread_mutex_unlock(&mutex);
			if(list->tail == NULL)
			{
				list->head = list->tail = node;
				list->head->prev = NULL;
				list->tail->next = NULL;
			}
			else/*exist node*/
			{	
				node->prev = list->tail;
				node->next = NULL;
				list->tail->next = node;

				list->tail = node;
			}
		
			list->count++;
			list->g_count++;//use thread amount
			add_num++;//make data number
		
			if(0 < list->count)
			{
				pthread_cond_broadcast(&ran_cond);
			}
			pthread_mutex_unlock(&mutex);
			printf("ADD Thread id is %ud, list->count : %d , list->g_count : %d\n", (int)id, list->count, list->g_count);
		}
		else
		{
			pthread_mutex_unlock(&mutex);
			break;
		}
	}
	printf("\n");
	printf("===================\n");
	printf("end add this thread\n");
	printf("====================\n");
	printf("\n");

	list->end_th++;
	return NULL;
}

void *ran2_delete_thread(void* data)
{
	pthread_t id;
	id = pthread_self();
	printf("%d delete thread ready\n",(int)id);

	//wait for start signal
	
	data_check* list = data;
	while(1)
	{
		pthread_mutex_lock(&mutex);
		if(list->g_count < MAX_TH)
		{
			//first check
			while(list->head == NULL && list->count == 0)
			{
				//printf("no data, Thread id is %ud\n", (int)id);
				pthread_cond_wait(&ran_cond,&mutex);//stop this thread(into mutex unlock form)
			}
			if(list->g_count >= MAX_TH)
			{
				pthread_mutex_unlock(&mutex);
				break;
			}
		
			node_t *cur; // find point place
			node_t *node;

			node = list->head;

			if(list->count == 1)
			{
				list->head = NULL;
				list->tail = NULL;
				//printf("delete %s data, Thread id is %ud",(char*)node->name, (int)id);

				free(node->name);
				free(node);

				--list->count;
			}
			else if(list->count != 1 && list->count != 0)
			{
				/*change head*/
				cur = list->head->next; //find second node
				cur->prev = NULL;
				list->head = cur;

				/*data delete*/
				node->prev = NULL;
				node->next = NULL;

				//printf("delete %s data, Thread id is %ud",(char*)node->name, (int)id);
				free(node->name);
				free(node);
			
				--list->count;
			}	
			
			list->g_count++;//execution thread amount
			pthread_mutex_unlock(&mutex);
			printf(" minus thread going check : %d check count : %d\n", list->g_count , list->count);
		}
		else
		{
			pthread_mutex_unlock(&mutex);
			break;
		}
	}
	printf("==========\n");
	printf("end delete thread\n");
	printf("===========\n");
	printf("\n");
	list->end_th++;
	
	return NULL;
}
